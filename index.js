console.log("Hello World!");
console.log("Hi World!");

// [SECTION] JavaScript Synchronous vs Asynchronous
// JavaScript is by default synchronous, meaning only one statement can be executed at a time.

// Asynchronous means that we can proceed to execute other statements. While consuming code is running in the background.

// [SECTION] Getting all posts
// Fetch API allows you to asynchronously request for a resource (data)

// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// // The "fetch" method will return a "promise" that resolves to a "response" object
// // The ".then" method captures the "response" object and returns another "promise" which will evenutally be resolved or rejected.
// fetch("https://jsonplaceholder.typicode.com/posts").then((response) =>
//   console.log(response.status)
// );

// fetch("https://jsonplaceholder.typicode.com/posts")
//   .then((response) => response.json())
//   .then((json) => console.table(json));

// async function fetchData() {
//   let result = await fetch("https://jsonplaceholder.typicode.com/posts");

//   console.log(result);
//   console.log(typeof result);

//   console.log(result.body);

//   let json = await result.json();
//   console.table(json);
// }

// fetchData();

// [SECTION] Creating a post

fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  headers: { "Content-type": "application/json" },
  body: JSON.stringify({
    title: "New post",
    body: "Hello World!",
    userId: "1",
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

// [SECTION] Updating post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: { "Content-type": "application/json" },
  body: JSON.stringify({
    id: 1,
    title: "Updated post",
    body: "Hello again",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.table(json));

//[SECTION] Deleting a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "DELETE",
});

// Retrieving nested/related comments to post
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
  .then((response) => response.json())
  .then((json) => console.table(json));
